const express = require('express');
const Task = require('../models/Task');
// const User = require('../models/User');
const auth = require("../middleware/auth");

const router = express.Router();


router.get('/', [auth], async (req, res) => {
    const tasks = await Task.find({username: req.user._id}).populate('user', 'username');
    try {
        res.send(tasks);
    } catch (e) {
        res.sendStatus(500)
    }
});


router.post('/', [auth], async (req, res) => {
    const taskData = req.body;
    const task = new Task(taskData);
    try {
        await task.save();
        res.send(task)
    } catch (e) {
        res.status(400).send(e);
    }
});

router.put('/:id', [auth], async (req, res) => {
    const taskData = req.body;

    try {
        const task = await Task.findOne({_id: req.params.id}); //.populate('user', 'username');
        task.status = taskData.status;
        task.title = taskData.title;
        task.description = taskData.description;
        task.username = taskData.username;
        await task.save();
        res.send(task);
        // return  res.sendStatus(404);
    } catch (e) {
        res.sendStatus(500);
    }


});

router.delete('/:id', [auth], async (req, res) => {
    try {
        await Task.deleteOne({_id: req.params.id}); // .populate('user', 'username');
        res.send("Removed")
        // res.sendStatus(404)
    } catch (e) {
        res.sendStatus(500)
    }
});

module.exports = router;