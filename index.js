const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const users = require('./app/users');
const tasks = require('./app/tasks');
const exitHook = require('async-exit-hook');


const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());


app.use('/users', users);
app.use('/tasks', tasks);


const port = 8000;


const run = async () => {
    await mongoose.connect('mongodb://localhost/todo',
        {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        });

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async (callback) => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error)



